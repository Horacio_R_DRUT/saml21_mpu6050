// I2Cdev library collection - Main I2C device class
// Abstracts bit and byte I2C R/W functions into a convenient class
// 6/9/2012 by Jeff Rowberg <jeff@rowberg.net>
// 11/28/2014 by Marton Sebok <sebokmarton@gmail.com>
//
// Changelog:
//     2014-11-28 - ported to PIC18 peripheral library from Arduino code
//		2020 - Ported to SAML21 / SAMR34  by Horacio R Drut

/* ============================================
I2Cdev device library code is placed under the MIT license
Copyright (c) 2013 Jeff Rowberg
Copyright (c) 2014 Marton Sebok
Copyright (c) 2017 Daichou
 *
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
===============================================
*/

#include "I2Cdev.h"


/** Read multiple bytes from an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr First register regAddr to read from
 * @param length Number of bytes to read
 * @param data Buffer to store read data in
 * @return Number of bytes read (-1 indicates failure)
 */
int8_t I2Cdev_readBytes(struct i2c_master_module *const module, uint8_t devAddr, uint8_t regAddr, uint8_t length, uint8_t *data) {
    enum status_code status;
	struct i2c_master_packet trans_packet;

	trans_packet.address		= devAddr;
	trans_packet.data_length	= 1;
	trans_packet.data			= &regAddr;
	trans_packet.ten_bit_address = false;
	trans_packet.high_speed	= false;

	 /* master sends AD+W and RA, no STOP and no ACK sent */
	status = i2c_master_write_packet_wait_no_stop(module, &trans_packet);
    if (status != STATUS_OK) {
	    return -1;
    }

	trans_packet.data_length	= length;
	trans_packet.data			= data;		// save what's going to be read
    /* Master sends AD+R, then sends NACK and STOP */
	status = i2c_master_read_packet_wait(module, &trans_packet);
	if (status != STATUS_OK) {
		return -1;
	}

    return length;
}

/** Read single byte from an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr Register regAddr to read from
 * @param data Container for byte value read from device
 * @return Status of read operation (true = success)
 */
int8_t I2Cdev_readByte(struct i2c_master_module *const module, uint8_t devAddr, uint8_t regAddr, uint8_t *data) {
    return I2Cdev_readBytes(module, devAddr, regAddr, 1, data);
}


/** Read a single bit from an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr Register regAddr to read from
 * @param bitNum Bit position to read (0-7)
 * @param data Container for single bit value
 * @return Status of read operation (true = success)
 */
int8_t I2Cdev_readBit(struct i2c_master_module *const module, uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t *data) {
    uint8_t b;
    uint8_t count = I2Cdev_readByte(module, devAddr, regAddr, &b);
    *data = b & (1 << bitNum);
    return count;
}

/** Read multiple bits from an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr Register regAddr to read from
 * @param bitStart First bit position to read (0-7)
 * @param length Number of bits to read (not more than 8)
 * @param data Container for right-aligned value (i.e. '101' read from any bitStart position will equal 0x05)
 * @return Status of read operation (true = success)
 */
int8_t I2Cdev_readBits(struct i2c_master_module *const module, uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t *data) {
    // 01101001 read byte
    // 76543210 bit numbers
    //    xxx   args: bitStart=4, length=3
    //    010   masked
    //   -> 010 shifted
    uint8_t count, b;
    if ((count = I2Cdev_readByte(module, devAddr, regAddr, &b)) != 0) {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        b &= mask;
        b >>= (bitStart - length + 1);
        *data = b;
    }
    return count;
}





/** Write multiple bytes to an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr First register address to write to
 * @param length Number of bytes to write
 * @param data Buffer to copy new data from
 * @return Status of operation (true = success)
 */
bool I2Cdev_writeBytes(struct i2c_master_module *const module, uint8_t devAddr, uint8_t regAddr, uint8_t length, uint8_t* data) {
    enum status_code status;
    struct i2c_master_packet trans_packet;

    trans_packet.address		= devAddr;
    trans_packet.data_length	= 1;
    trans_packet.data			= &regAddr;
    trans_packet.ten_bit_address = false;
    trans_packet.high_speed	= false;

    /* master sends AD+W and RA, no STOP and no ACK sent */
    status = i2c_master_write_packet_wait_no_stop(module, &trans_packet);
    if (status != STATUS_OK) {
	    return false;
    }

	// TODO: implement burst write.
	trans_packet.data			= data;
	/* master sends DATA, then STOP */
	//status = i2c_master_write_packet_wait(module, &trans_packet);
	status = i2c_master_write_byte(module, *data);
	if (status != STATUS_OK) {
		return false;
	}
	i2c_master_send_stop(module);

    return true;
}

/** Write single byte to an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr Register address to write to
 * @param data New byte value to write
 * @return Status of operation (true = success)
 */
bool I2Cdev_writeByte(struct i2c_master_module *const module, uint8_t devAddr, uint8_t regAddr, uint8_t data) {
    return I2Cdev_writeBytes(module, devAddr, regAddr, 1, &data);
}


/** write a single bit in an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr Register regAddr to write to
 * @param bitNum Bit position to write (0-7)
 * @param value New bit value to write
 * @return Status of operation (true = success)
 */
bool I2Cdev_writeBit(struct i2c_master_module *const module, uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t data) {
    uint8_t b;
    I2Cdev_readByte(module, devAddr, regAddr, &b);
    b = (data != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum));
    return I2Cdev_writeByte(module, devAddr, regAddr, b);
}


/** Write multiple bits in an 8-bit device register.
 * @param devAddr I2C slave device address
 * @param regAddr Register regAddr to write to
 * @param bitStart First bit position to write (0-7)
 * @param length Number of bits to write (not more than 8)
 * @param data Right-aligned value to write
 * @return Status of operation (true = success)
 */
bool I2Cdev_writeBits(struct i2c_master_module *const module, uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t data) {
    //      010 value to write
    // 76543210 bit numbers
    //    xxx   args: bitStart=4, length=3
    // 00011100 mask byte
    // 10101111 original value (sample)
    // 10100011 original & ~mask
    // 10101011 masked | value
    uint8_t b;
    if (I2Cdev_readByte(module, devAddr, regAddr, &b) != 0) {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        data <<= (bitStart - length + 1); // shift data into correct position
        data &= mask; // zero all non-important bits in data
        b &= ~(mask); // zero all important bits in existing byte
        b |= data; // combine data with existing byte
		return I2Cdev_writeByte(module, devAddr, regAddr, b);
    } else {
        return false;
    }
}
