** MPU6050 accelerometer + gyroscope I2C driver for SAML21 family **

May also work with SAMD21, and will definitely work with SAMR34 chips.

The gyro functions have been stripped out since I didn't need them, but you can easily add them from: 

https://github.com/jrowberg/i2cdevlib

Any comments or suggestions feel free to leave me a message.


